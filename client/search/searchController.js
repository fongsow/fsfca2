/**
 * Created by s17465 on 8/11/2016.
 */
/* Search Controller */

(function () {
    'use strict';

    angular
        .module("ProductApp")
        .controller("SearchCtrl", SearchCtrl);

    SearchCtrl.$inject = ["$state","SearchService"];

    function SearchCtrl($state, SearchService) {

        var vm = this;

        vm.searchTerm = "";
        vm.products = [];
        product: {}

        vm.searchByProduct = searchByProduct;
        vm.searchByBrand = searchByBrand;
        vm.showProduct = showProduct;

        init();

        function init() {
            SearchService.init()
                .then(function(result){
                    vm.products = result;
                })
                .catch(function(error){
                    console.log("Search Init Error")
                })
        };

        function searchByProduct() {
            SearchService.searchByProduct(vm.searchTerm)
                .then(function(result){
                    vm.products = result;
                })
                .catch(function(error){
                    console.log("Search Init Error")
                })
        }

        function searchByBrand() {
            SearchService.searchByBrand(vm.searchTerm)
                .then(function(result){
                    vm.products = result;
                })
                .catch(function(error){
                    console.log("Search Init Error")
                })
        }

        function showProduct(prodID) {
            console.log("OK");
            console.log(prodID);
            $state.go("A1",{prodID : prodID});
        }
    }
})();
