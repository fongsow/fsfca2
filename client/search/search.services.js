/**
 * Created by s17465 on 8/11/2016.
 */
(function () {
    angular
        .module("ProductApp")
        .service("SearchService", SearchService);

    SearchService.$inject = ["$http", "$q"];

    function SearchService($http, $q) {
        var vm = this;

        vm.init = function () {
            var defer = $q.defer();
            $http
                .get("/api/products")
                .then(function (response) {
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    console.log("error " + err);
                    defer.reject(err);
                });
            return (defer.promise);
        }

        vm.searchByProduct = function (searchTerm) {
            var defer = $q.defer();
            $http
                .get("/api/products/",
                     {params: {'productName':searchTerm}})
                .then(function (response) {
                    console.log("inside searchby Proudct");
                    console.log(response.data);
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    console.log("error " + err);
                    defer.reject(err);
                });
            return (defer.promise);
        }

        vm.searchByBrand = function (searchTerm) {
            var defer = $q.defer();
            $http
                .get("/api/brands/",
                    {params: {'brandName':searchTerm}})
                .then(function (response) {
                    console.log("inside searchby Brand Name");
                    console.log(response.data);
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    console.log("error " + err);
                    defer.reject(err);
                });
            return (defer.promise);
        }
    }
})();
