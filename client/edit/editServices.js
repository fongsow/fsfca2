/**
 * Created by s17465 on 8/11/2016.
 */
(function () {
    angular
        .module("ProductApp")
        .service("EditService", EditService);

    EditService.$inject = ["$http", "$q"];


    function EditService($http, $q) {
        var vm = this;
        vm.getProduct = getProduct;
        vm.updateProduct = updateProduct;

        function getProduct (searchTerm) {
            var defer = $q.defer();
            var pid = searchTerm;
            console.log("get product service");
            console.log(pid);

            $http
                .get("/api/oneprod/" + pid)
                .then(function (response) {
                    console.log("inside getProduct", response);
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    console.log("error " + err);
                    defer.reject(err);
                });
            return (defer.promise);
        };

        function getProductX(searchTerm) {
            var defer = $q.defer();

            $http
                .get("/api/oneprod/",
                    {params: {'prodID':searchTerm}})
                .then(function (response) {
                    console.log("inside getProduct", response);
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    console.log("error " + err);
                    defer.reject(err);
                });
            return (defer.promise);
        };

        function updateProduct(item) {
            var defer = $q.defer();
            $http
                .put("/api/products",item)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    console.log("error " + err);
                    defer.reject(err);
                });
            return (defer.promise);
        }

    }
})();
