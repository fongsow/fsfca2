/**
 * Created by s17465 on 8/11/2016.
 */
/* Edit Controller */

(function () {
    'use strict';

    angular
        .module("ProductApp")
        .controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["$stateParams", "EditService"];

    function EditCtrl($stateParams, EditService) {
        var vm = this;

        vm.result = {};
        vm.result.id = 2; //testing only
        vm.result.name = "";
        vm.result.brand = ""
        vm.result.upc12 = 0;
        vm.updateProduct = updateProduct;
        vm.search = search;

        if ($stateParams.prodID) {
            console.log("in Edit Controller")
            vm.result.id = Number($stateParams.prodID);
            console.log(vm.result.id);
            //vm.search() //why not working
            initResult();
        }
        function initResult() {
            vm.result.id = 2; //testing only
            vm.result.name = "xxxx";
            vm.result.brand = "Caress"
            //vm.result.upc12 = 11111065925;
        };

        function updateProduct() {
            EditService.updateProduct(vm.result)
                .then(function (result) {
                    console.log("update OK");
                    initResult();
                })
                .catch(function (error) {
                    alert("Update Error")
                    console.log("Search Init Error")
                })

        };

        function search() {
            EditService.getProduct(vm.result.id)
                .then(function (response) {
                    console.log("update OK");
                    vm.result.name = response.data.name;
                    vm.result.brand = response.data.brand;
                    vm.result.upc12 = response.data.upc12l

                })
                .catch(function (error) {
                    alert("Search Error")
                    console.log("Search Init Error")
                })

        };

    }
})();