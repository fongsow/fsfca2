/**
 * Created by s17465 on 8/11/2016.
 */
(function () {
    angular
        .module("ProductApp")
        .config(prodAppConfig);

    prodAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function prodAppConfig($stateProvider,$urlRouterProvider){

        $stateProvider
            .state('A',{
                url : '/A',
                templateUrl :'./search/search.html',
                controller : 'SearchCtrl',
                controllerAs : 'vm'
            })
            .state('B',{
                url : '/B',
                templateUrl :'./edit/editProduct.html',
                controller : 'EditCtrl',
                controllerAs : 'vm'
            })
            .state('A1',{
                url : '/B/:prodID',
                templateUrl :'./edit/editProduct.html',
                controller : 'EditCtrl',
                controllerAs : 'vm'
            })

        $urlRouterProvider.otherwise("/A");

    }
})();