/**
 * Created by s17465 on 8/11/2016.
 */

var express = require("express");
var path = require("path");
var ProductController = require('./api/product.controller')

const docroot = path.join(__dirname, "..");
const CLIENT_FOLDER = path.join(docroot, "client");

module.exports = {
    init: configureRoutes,
    errorHandler: errorHandler
}

function configureRoutes(app){
    /* APIs */
    //app.put('/api/products', ProductController.updateProduct);
    //app.get('/api/products/:brand', ProductController.searchByBrand);
    //app.get('/api/products/:prodname', ProductController.searchByProduct);

    app.put('/api/products', ProductController.updateProduct);
    app.get('/api/oneprod/:pid', ProductController.getProduct);
    app.get('/api/products', ProductController.productList);
    app.get('/api/brands', ProductController.searchByBrand);


    /* Static Files */
    app.use(express.static(CLIENT_FOLDER));
    app.use("/bower_components",
        express.static(path.join(docroot, "bower_components")));

}

function errorHandler(app) {
    app.use(function (req, res) {
        res.status(401).sendFile(CLIENT_FOLDER + "/app/errors/404.html");
    });

    app.use(function (err, req, res, next) {
        res.status(500).sendFile(path.join(CLIENT_FOLDER + '/app/errors/500.html'));
    });
};

