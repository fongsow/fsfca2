/*
File: app.js
Purpose: Entry Point of the project
 */

//Load express
var express = require("express");

//Create an instance of express application
var app = express();
var routes = require("./routes");
var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//Routes
routes.init(app);
routes.errorHandler(app);
console.log("OK");

//Start the web server on port 3000
app.listen(3000, function () {
    console.info("Webserver started on port 3000");
});

