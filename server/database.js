/**
 * Created by s17465 on 8/11/2016.
 */
var Sequelize = require("sequelize");
var config = require('./config');

var database = new Sequelize(
    config.mysql.database,
    config.mysql.username,
    config.mysql.password, {
        host: config.mysql.host,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    });

var ProductModel = require('./model/product.model')(database);

module.exports = {
    ProductTbl: ProductModel
};