/**
 * Created by s17465 on 8/11/2016.
 */
var Product = require('../database').ProductTbl;

module.exports = {
    productList: function (req, res) {

        //console.log("inside productList");

        var where = {};
        if (req.query.productName) {
            where.name = {
                $like: "%" + req.query.productName + "%"
            }
        }
        //console.log(where);

        Product
            .findAll({
                where: where,
                order: 'name',
                limit: 20
            })
            .then(function (products) {
                console.log("find OK");
                res.json(products);
            })
            .catch(function () {
                res.status(500).json({error: true})
            });
    },

    searchByBrand: function(req, res) {
        //console.log("brand listing");

        var where = {};
        if (req.query.brandName) {
            where.brand = {
                $like: "%" + req.query.brandName + "%"
            }
        }
        //console.log(where);

        Product
            .findAll({
                where: where,
                order: 'brand',
                limit: 20
            })
            .then(function (products) {
                console.log("find OK");
                res.json(products);
            })
            .catch(function () {
                res.status(500).json({error: true})
            });

    },

    updateProduct: function(req, res) {
        console.log("Insider update product");
        console.log(req.body);

        var where = {};
        where.id = req.body.id;

        // Updates Product Details
        Product
            .update(
                {name: req.body.name,
                 brand: req.body.brand},
                {where: where}         // search condition / criteria
            ).then(function(result) {
                res.json(result);
            }).catch(function(err){
                console.log("Update Error");
                res.status(500).json({"error": err})
            })
    },

    getProduct: function(res, res) {
        var where = {};

        //where.id = request.query.prodID;
        where.id = request.params.pid;

        //console.log(where);
        res.json({'OK': 'OK'})
        /*
        Product
            .findOne({
                where: where,
            })
            .then(function (product) {
                console.log("find OK");
                res.json(product);
            })
            .catch(function () {
                res.status(500).json({error: true})
            });
        */
    }
}


