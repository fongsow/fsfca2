//Create a model for departments table
var Sequelize = require("sequelize")

module.exports = function (conn) {
    var Product = conn.define("grocery_list", {
        upc12: {type: Sequelize.INTEGER,  allowNull: false,},
        brand: {type: Sequelize.STRING,  allowNull: false},
        name: {type: Sequelize.STRING,  allowNull: false,},
    }, {
        tableName: 'grocery_list',
        timestamps: false
    });

    return Product;
};